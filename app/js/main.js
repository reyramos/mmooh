// Require JS  Config File
require( {
			 baseUrl: '/js/',
			 paths: {
				 'angular': '../lib/angular/angular'
				 , 'angular-route': '../lib/angular-route/index'
				 , 'angular-sanitize': '../lib/angular-sanitize/index'
				 , 'angular-animate': '../lib/angular-animate/index'
				 , 'angular-touch': '../lib/angular-touch/index'
				 , 'jquery': '../lib/jquery/dist/jquery'
				 , 'froogaloop2':'../lib/froogaloop2/index'
				 , 'modalProvider':'../lib/ngModalProvider/ngModalProvider'
			 }, map: {
				 '*': { 'jquery': 'jquery' }, 'noconflict': { "jquery": "jquery" }
			 }, shim: {
				 'app': { 'deps': [
					   'angular'
					 , 'angular-route'
					 , 'angular-sanitize'
					 , 'angular-animate'
					 , 'angular-touch'
					 , 'modalProvider'
				 ]}
				 , 'angular-route': { 'deps': ['angular', 'jquery'], exports: 'angular' }
				 , 'angular-sanitize': { 'deps': ['angular'] }
				 , 'angular-animate': { 'deps': ['angular'] }
				 , 'modalProvider': { 'deps': ['angular'] }
				 , 'angular-touch': { 'deps': ['angular'] }
				 , 'jquery': {
					 init: function ( $ ) {
						 return $.noConflict( true );
					 },
					 exports: 'jquery'
				 }
				 , 'routes': { 'deps': [
					 'app'
				 ]}
				, 'filters/globalFilters': { 'deps': ['app'] }
				, 'controllers/ApplicationController': {
					 'deps': [
						 'app'
				 ]}
				 , 'directive/vimeo': {
					 'deps': [
						 'app','froogaloop2'
					 ]}
				 , 'controllers/videoController': {
					 'deps': [
						 'app'
						 ,'services/appDataService'
					 ]}
				 , 'services/appDataService': { 'deps': [
					 'app'
				 ]}
				}
			 }
		, [
			 'require'
			 , 'routes'
			 , 'filters/globalFilters'
			 , 'services/appDataService'
			 , 'controllers/ApplicationController'
			 , 'controllers/videoController'
			 , 'directive/vimeo'

		 ]
	, function ( require ) {
		return require(
			[
				'bootstrap'
			]
		)
	}
);