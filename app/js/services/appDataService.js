/**
 * # Global addDataService
 *
 * This will run before any controller to gather the
 * necessary data. It will call initApplication to gather
 * the RESTful services.
 *
 * This is an example of a Singleton Pattern within Angular
 *
 */

	// @TODO: Move this to enums directory and rename to globalEnums.
angular.module('app').service
	( 'appDataService',[
		'$q'
		, '$log'
		, '$http'
		, function(
			$q
			, $log
			, $http
			) {


			var  appDataService = []
			/**
			 * Initialize application and run before any controller
			 *
			 * @returns {promise}
			 */
			this.initApplication = function(){
				var defer = $q.defer()


				$http({method: 'GET', url: 'http://vimeo.com/api/v2/monstermediaooh/all_videos.json'}).
					success(function(data, status, headers, config) {
							appDataService = data
							defer.resolve();
					}).
					error(function(data, status, headers, config) {
							console.log('ERROR')
							console.log(data, status, headers, config)
				 });

				return defer.promise;
			}

			//getters
			this.getRequest  = function(){
				return appDataService;
			}


		}]
	)