/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'ApplicationController',
	[
		 '$scope'
		 , '$rootScope'
		, '$location'
		, function
		(
			  $scope
			, $rootScope
			, $location
			) {

		$scope.routeTo = function( page ){

			if (typeof page == 'undefined') {
				$location.path( '/' );
			} else {
				$location.path( page );
			}
		};

	}]
)
