/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'videoController',
	[
		'$scope'
		, '$log'
		, 'appDataService'
		, '$routeParams'
		, 'modalProvider'
		, '$templateCache'
		, function
			(
				$scope
				, $log
				, appDataService
				, $routeParams
				, modalProvider
				, $templateCache
				) {
		//			<vimeo control-boolean="isPlaying" vid="98646917" pid="1"></vimeo>
		//			<p><button ng-click="play()">Play</button> <button ng-click="pause()">Pause</button></p>


		$scope.videos = appDataService.getRequest();
		$scope.isPlaying = false




		var myTemplate = '<div class="modal fade">'
			+ '<div class="modal-dialog">'
			+ '<div class="modal-content">'
			+ '<div class="modal-header">'
			+ '<button type="button" class="close" data-ng-click="modal.hide()">&times;</button>'
			+ '<h4 class="modal-title" data-ng-bind="modal.title"></h4>'
			+ '</div>'
			+ '<div class="modal-body" data-modal-body></div>'
			+ '</div></div></div>';

		$scope.videoDisplay = function(obj){

			var videoModal = angular.element(myTemplate),
				modalBody = videoModal[0].querySelector("[data-modal-body]" );

			angular.element(modalBody).append('<vimeo control-boolean="isPlaying" width="'+obj.width+'" height="'+obj.height+'" vid="'+obj.id+'" pid="'+obj.id+'"></vimeo>')

			//replacing modalProvider template
			$templateCache.put('/ngModalProvider/modal.html', "<div class='modal fade'>"+videoModal[0].innerHTML+"</div>");

			var modal = {
				  title:obj.title
			}

			modalProvider.set(modal);

		}

	}]
)
