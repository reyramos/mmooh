<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 6/20/14
* Time: 9:17 AM
*/
ini_set('display_errors', 'On');
error_reporting(E_ALL);

header('Access-Control-Allow-Headers: Content-Type');

include_once __DIR__ . "/vimeo-php-lib/vimeo.php";


$config = json_decode(file_get_contents('config.json'), true);

$lib = new phpVimeo($config['client_id'], $config['client_secret']);

$videos = $lib->call('vimeo.videos.getAll', array('user_id' => 'monstermediaooh', 'full_response' => true));
$array = json_decode(json_encode($videos), true);

//file_put_contents('13340709_videos.json',json_encode($user));
echo json_encode($array);

