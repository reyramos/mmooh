<?php

ini_set('display_errors', 'On');
error_reporting(E_ALL);

header('Access-Control-Allow-Headers: Content-Type');

include_once __DIR__."/vimeo-php/vimeo.php";


$config = json_decode(file_get_contents('config.json'), true);

$lib = new Vimeo($config['client_id'], $config['client_secret']);

if (!empty($config['access_token'])) {
	$lib->setToken($config['access_token']);
//	$user = $lib->request('/me');
	$user = $lib->request('/users/13340709/videos');

} else {
	echo "missing access_token";
}

echo json_encode($user);

