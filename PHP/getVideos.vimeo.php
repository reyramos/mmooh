<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 6/19/14
 * Time: 1:21 PM
 */
ini_set('display_errors', 'On');
error_reporting(E_ALL);

include_once __DIR__ . "/vimeo-php-lib/vimeo.php";

unset($_SESSION);

class getVideos
{
	protected static $config;
	protected static $vimeo;

	private static $summary = array();
	private static $videos = array();

	public function __construct(){}


	public static function configure($configuration)
	{

		if (file_exists($configuration)) {
			self::$config = json_decode(file_get_contents($configuration), true);

			self::$vimeo = new phpVimeo(self::$config['client_id'], self::$config['client_secret']);

			if( array_key_exists('user_id',self::$config))
			self::getAll(self::$config['user_id']);

		} else {
			//some error
			throw new Exception($configuration." missing");
		}

	}


	/**
	 * Pass the user_id to get all the public videos in vimeo
	 * @param string $user_id (required) The ID number or username of the user. A token may be used instead.
	 * @throws Exception
	 */
	public static function getAll($user_id = "")
	{
		$page = 1;
		$ceil = null;
		$generated_in = 0;
		$total = 0;

		if($user_id == ""){
			throw new Exception("missing user_id");
		}


		do {

			$videos = self::$vimeo->call('vimeo.videos.getAll', array('user_id' => $user_id, 'page' => $page, 'full_response' => true));
			$array = json_decode(json_encode($videos), true);
			$generated_in += floatval($array['generated_in']);

			if ($ceil == null) {
				$total = $array['videos']['total'];
				$ceil = ceil($total / 50);
			}

			self::$videos = array_merge(self::$videos, $array['videos']['video']);

			$page++;

		} while ($page <= $ceil);

		self::$summary = array(
			'generated_in' => $generated_in,
			'total' => $total
		);

	}

	public function getSummary()
	{
		return self::$summary;
	}

	public function getVideos()
	{
		return self::$videos;
	}

}

getVideos::configure('config.json');


$mm = new getVideos();
var_dump($mm->getSummary());
var_dump($mm->getVideos());





